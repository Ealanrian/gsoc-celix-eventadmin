/**
 *Licensed to the Apache Software Foundation (ASF) under one
 *or more contributor license agreements.  See the NOTICE file
 *distributed with this work for additional information
 *regarding copyright ownership.  The ASF licenses this file
 *to you under the Apache License, Version 2.0 (the
 *"License"); you may not use this file except in compliance
 *with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *Unless required by applicable law or agreed to in writing,
 *software distributed under the License is distributed on an
 *"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 *specific language governing permissions and limitations
 *under the License.
 */
/*
 * event.h
 *
 *  Created on: Jul 19, 2013
 *      Author: erik
 *  	copyright	Apache License, Version 2.0
 *		note: Create for Google Summer of Code 2013
 */


#ifndef EVENT_H_
#define EVENT_H_
#include <apr_general.h>
#include "celix_errno.h"
#include "properties.h"
#include "array_list.h"

typedef struct event *event_pt;
/**
 * @desc create an event
 * @param char *topic. String containing the topic
 * @param properties_pt properties.
 */
celix_status_t createEvent(char *topic, properties_pt properties, apr_pool_t *pool, event_pt *event);
/**
 * @desc checks if an event contains the property
 * @param event_pt *event. the event to check
 * @param char *property. the key for the property to check
 * @param bool *result. the result.
 */
celix_status_t containsProperty(event_pt *event, char *property, bool *result);
/**
 * @desc checks if an event is equal to the second event.
 * @param event_pt *event, event to compare to
 * @param event_pt *event, the event to be compared
 * @param bool *result. the result true if equal.
 */
celix_status_t event_equals(event_pt *event, event_pt *compare, bool *result);
/**
 * @desc gets a property from the event.
 * @param event_pt *event. the event to get the property from
 * @param char *propertyKey the key of the property to get
 * @param char **propertyValue. the result param will contain the property if it exists in the event.
 */
celix_status_t getProperty(event_pt *event, char *propertyKey, char **propertyValue);
/**
 * @desc gets all property names from the event
 * @param event_pt *event. the event to get the property names from
 * @param array_list_pt *names. will contain the keys
 */
celix_status_t getPropertyNames(event_pt *event, array_list_pt *names);
/**
 * @desc gets the topic from an event
 * @param event_pt *event. the event to get the topic from
 * @param char **topic, result pointer will contain the topic.
 */
celix_status_t getTopic(event_pt *event, char **topic);
celix_status_t hashCode(event_pt *event, int *hashCode);
celix_status_t matches(event_pt *event);
celix_status_t toString(event_pt *event, char *eventString);

#endif /* EVENT_H_ */
