------------------------------------------------------------------
# This project is the GSOC Event admin project for apache celix.
This project was started by Erik Jansman during GSOC 2013 with Pepijn Noltes as mentor.
During Google summer of code the event admin was implemented. The code as it stands now is not yet complete but a lot of the spec is done. 
The bundles are to be build on a system with Celix installed. In a framework there should only be one event admin with multiple event publishers and handler. In this project one publisher and one handler was build to demonstrate the workings of the project.
